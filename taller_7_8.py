import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

df = pd.read_csv("pregunta8.csv")
#print(xl.sheet_names)
#df = xl.parse("Data")

print(df.head())
print(df.describe())
#print(df)

#df['Type of\nRepair']=df['Type of\nRepair'].mask(df['Type of\nRepair'] == 0,'Electric')
#df['Type of\nRepair']=df['Type of\nRepair'].mask(df['Type of\nRepair'] == 1,'Mecanic')
#df.to_csv('pregunta8.csv')


# print(df.describe())

dummy_repair= pd.get_dummies(df['Type of\nRepair'])
dummy_who= pd.get_dummies(df['Who'])
print(dummy_repair)
print(dummy_who)

df = pd.concat([df,dummy_repair,dummy_who],axis=1);
#df = pd.concat(df,dummy_who);

print(df)

sns.boxplot(x='Type of\nRepair', y='Repair Time\n(hours)', data=df)
sns.boxplot(x='Who', y='Repair Time\n(hours)', data=df)
plt.show()

print(df[["Months Since\nLast Service", "Type of\nRepair","Repair Time\n(hours)","Who","Electric" , "Mecanic"  ,"Andres"  ,"Carlos"]].corr())

X = df[["Months Since\nLast Service","Electric" , "Mecanic"  ,"Andres"  ,"Carlos"]]
Y = df["Repair Time\n(hours)"]

regr = linear_model.LinearRegression()

# # # #Train the model using the training sets
regr.fit(X,Y)
print(regr.coef_)

print("Y = ")
for i in range(len(regr.coef_)):
	print("x"+str(i)+"*" + str(regr.coef_[i]))
print(" + "+ str(regr.intercept_))

# result =78*regr.coef_[0]+175*regr.coef_[1] +1*regr.coef_[2] +0*regr.coef_[3] + regr.intercept_
# print(result)
# #print(regr.intercept_)

