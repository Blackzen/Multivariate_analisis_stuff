import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

xl = pd.ExcelFile("Stroke.xls")
print(xl.sheet_names)
df = xl.parse("Data")

dummy= pd.get_dummies(df.Smoker)

df = pd.concat([df,dummy],axis=1)
print(df.head())
print(df.describe())


sns.boxplot(x="Smoker", y="Risk", data=df)
plt.show()

print(df[["Risk", "Age","Pressure","Yes","No"]].corr())
pearson_coef, p_value = stats.pearsonr(df['Risk'], df['Yes'])
print(pearson_coef)
print(p_value)
#pearson_coef, p_value = stats.pearsonr(df['Risk'], df['No'])

#sns.boxplot(x="Age", y="Risk", data=df)
#sns.boxplot(x="Age", y="Risk", data=df)

X = df[["Age","Pressure","Yes","No"]]
Y = df["Risk"]

regr = linear_model.LinearRegression()

# # #Train the model using the training sets
regr.fit(X,Y)
print(regr.coef_)

print("Y = ")
for i in range(len(regr.coef_)):
	print("x"+str(i)+"*" + str(regr.coef_[i]))

print(" + "+ str(regr.intercept_))

result =78*regr.coef_[0]+175*regr.coef_[1] +1*regr.coef_[2] +0*regr.coef_[3] + regr.intercept_
print(result)
#print(regr.intercept_)

Y_hat = regr.predict(X)

plt.figure()

ax1 = sns.distplot(Y, hist=False, color="r", label="Actual Value")
sns.distplot(Y_hat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted')
plt.xlabel('Parametros')
plt.ylabel('Risk')

plt.show()
