import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

xl = pd.ExcelFile("ForFunds.xls")
print(xl.sheet_names)
df = xl.parse("Data")
print(df.head())

print(df.describe())
print(df[["Safety Rating", "Annual Expense Ratio (%)","Return (%)"]].corr())

X = df[["Safety Rating", "Annual Expense Ratio (%)"]]
Y = df["Return (%)"]

#sns.regplot(X, Y, data=df)
#plt.show()

regr = linear_model.LinearRegression()

# # #Train the model using the training sets
regr.fit(X,Y)
print(regr.coef_)

print("Y = ")
for i in range(len(regr.coef_)):
	print("x"+str(i)+"*" + str(regr.coef_[i]))

print(" + "+ str(regr.intercept_))

result =7.5*regr.coef_[0]+2*regr.coef_[1] + regr.intercept_
print(result)
#print(regr.intercept_)

Y_hat = regr.predict(X)

plt.figure()

ax1 = sns.distplot(Y, hist=False, color="r", label="Actual Value")
sns.distplot(Y_hat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted')
plt.xlabel('Parametros')
plt.ylabel('Retorno')

plt.show()
