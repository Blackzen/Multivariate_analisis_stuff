# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

df = pd.read_csv("data2.csv")
print(df.head())

# sns.regplot(x="Indice", y="Salario", data=df)
# sns.residplot(df['Indice'], df['Salario'])
# plt.show()

sns.residplot(df['Indice'], df['Salario'])
plt.show()



print(df[["Indice","Salario"]].corr())

X = df["Indice"]
Y = df["Salario"]

#results = sm.OLS(Y,X).fit()
#predictions = results.predict([50])
#print(predictions)
##NO LOGRO IDENTICAR EL INTERCEPTO B0 Y B1
#print(results.summary())

regr = linear_model.LinearRegression()

#Train the model using the training sets
regr.fit(X.values.reshape(-1,1),Y)

print("b: " + str(regr.intercept_))
print("m: " + str(regr.coef_))

result = regr.intercept_ + regr.coef_*50
print(result)
plt.show()


