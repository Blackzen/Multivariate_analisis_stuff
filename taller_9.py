import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

xl = pd.ExcelFile("Lakeland.xls")
#print(xl.sheet_names)
df = xl.parse("Data")

print(df.head())
print(df.describe())

#sns.boxplot(x='Program', y='Return', data=df)

sns.regplot(x="GPA", y="Return", data=df)

plt.show()
print(df)


X = df[["GPA" ,'Program', 'Return' ]]
Y = df["Return"]

regr = linear_model.LinearRegression()

# # # #Train the model using the training sets
regr.fit(X,Y)
print(regr.coef_)
print(regr.intercept_)

Y_hat = regr.predict(X)

plt.figure()

ax1 = sns.distplot(Y, hist=False, color="r", label="Actual Value")
sns.distplot(Y_hat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted')
plt.xlabel('Parametros')
plt.ylabel('Return')

plt.show()

