# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

#import sklearn
#from sklearn.linear_model import LinearRegression
df = pd.read_csv("data.csv")
print(df.head())

X = df["Experiencia"]
Y = df["Ventas-Anuales"]
#df.plot(x='Experiencia', y='Ventas-Anuales', kind='scatter')
#plt.show()

sns.regplot(x="Experiencia", y="Ventas-Anuales", data=df)
print(df[["Experiencia", "Ventas-Anuales"]].corr())

results = sm.OLS(Y,X).fit()
predictions = results.predict([9])

print(predictions)

############
#SCILEARN REGRESSION STUFF
# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(X.values.reshape(-1,1),Y)

print(regr.intercept_)
print(regr.coef_)


#Y = mX + b
# plt.ylim(0,)
plt.show()

##NO LOGRO IDENTICAR EL INTERCEPTO B0 Y B1
print(results.summary())


