import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score


df = pd.read_csv("data3.csv",decimal= ",") 
print(df.head())

print(df.describe())


# sns.regplot(x="Promedio", y="Salario", data=df)
# print(df[["Promedio","Salario"]].corr())

X = df["Promedio"]
Y = df["Salario"]

regr = linear_model.LinearRegression()

# #Train the model using the training sets
regr.fit(X.values.reshape(-1,1),Y)

print("b: " + str(regr.intercept_))
print("m: " + str(regr.coef_))
print("Y = "+ str(regr.coef_) +"*X + " +str(regr.intercept_))
#result = regr.intercept_ + regr.coef_*50
#print(result)


Y_predict_multifit = regr.predict(X.values.reshape(-1,1))
print(Y_predict_multifit)

print(Y.mean())


SC=pow((Y - Y_predict_multifit),2)

SCE=(SC.sum())
print SCE

SCTU=pow((Y - Y.mean()),2)
print SCTU

SCT=SCTU.sum()

print SCT

R=regr.score(X.values.reshape(-1,1),Y)
print R


# print(pow(X,2).sum())
# print(pow(Y,2).sum())
# print((np.multiply(X,Y)).sum())

# Sx = X.sum()
# Sxx = Y.sum()
# Sy = pow(X,2).sum()
# Syy = pow(Y,2).sum()
# Sxy = (np.multiply(X,Y)).sum()


#results = sm.OLS(Y,X).fit()
#NO ENTIENDO TODA LA INFORMACION EN LA TABLA
#print(results.summary())

#scatter plot
sns.regplot(x="Promedio", y="Salario", data=df)

#residual plot
#sns.residplot(X,Y)

Y_hat = regr.predict(X.values.reshape(-1,1))

plt.figure()

#distribution plot
ax1 = sns.distplot(Y, hist=False, color="r", label="Actual Value")
sns.distplot(Y_hat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted')
plt.xlabel('Promedio')
plt.ylabel('Salario')

plt.show()

