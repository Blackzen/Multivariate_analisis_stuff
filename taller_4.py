import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import statsmodels.api as sm

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score


df = pd.read_csv("data4.csv",decimal= ",") 
print(df.head())

print(df.describe())
print(df[["Manejo","Confiabilidad","Ajuste"]].corr())

X = df[["Manejo","Confiabilidad","Ajuste"]]
Y = df["General"]

regr = linear_model.LinearRegression()

# #Train the model using the training sets
regr.fit(X,Y)
print(regr.coef_)

# print("Y = ")
# for i in range(len(regr.coef_)):
# 	print("x"+str(i)+"*" + str(regr.coef_[i]))

# print(" + "+ str(regr.intercept_))
for x in range(len(regr.coef_)):
	print(regr.coef_[x])

result =8.28*regr.coef_[0]+9.06*regr.coef_[1]+8.07*regr.coef_[2] + regr.intercept_
print(result)
print(regr.intercept_)

Y_hat = regr.predict(X)

plt.figure()

ax1 = sns.distplot(Y, hist=False, color="r", label="Actual Value")
sns.distplot(Y_hat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted')
plt.xlabel('parametros')
plt.ylabel('Calidad general')

plt.show()


#result = regr.intercept_ + regr.coef_*50
#print(result)

